﻿using System;

namespace Fibonacci_ForLoop
{
    class Program
    {
        static void Main(string[] args)
        {
            //Calculate Fibonacci sequence to 7 digits with a For-Loop
            int[] number = new int[7];
            for(int i = 0; i < number.Length; i++)
            {
                if(i == 0 || i == 1)
                {
                    number[i] = 1;
                }
                else
                {
                    number[i] = number[i - 1] + number[i - 2];
                }
                Console.WriteLine(number[i]);
            }
        }
    }
}
